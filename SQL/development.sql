drop database learnplatform;
create database learnplatform;
USE learnplatform;

DROP TABLE IF EXISTS `NodeExercise`;
DROP TABLE IF EXISTS `ExerciseSolved`;
DROP TABLE IF EXISTS `Exercise`;
DROP TABLE IF EXISTS `Difficulty`;
DROP TABLE IF EXISTS `GroupPermission`;
DROP TABLE IF EXISTS `PermissionType`;
DROP TABLE IF EXISTS `Node`;
DROP TABLE IF EXISTS `UserGroup`;
DROP TABLE IF EXISTS `Group`;
DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    email VARCHAR(255) UNIQUE,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `Group` (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    creatorId INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (creatorId)
        REFERENCES User (id)
);

CREATE TABLE `UserGroup` (
    userId INT NOT NULL,
    `groupId` INT NOT NULL,
    PRIMARY KEY (userId , `groupId`),
    FOREIGN KEY (userId)
        REFERENCES User (id),
    FOREIGN KEY (`groupId`)
        REFERENCES `Group` (id)
        ON DELETE CASCADE
);

CREATE TABLE `Node` (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    parentNodeId INT,
    creatorId INT,
    PRIMARY KEY (id),
    FOREIGN KEY (parentNodeId)
        REFERENCES Node (id)
        ON DELETE CASCADE,
	FOREIGN KEY (creatorId)
		REFERENCES User (id)
);

CREATE TABLE `PermissionType` (
    id INT NOT NULL,
    name VARCHAR(255) NOT NULL UNIQUE,
    `rank` INT NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

INSERT INTO `PermissionType` VALUES (1, "Admin", 0);

CREATE TABLE `GroupPermission` (
    `groupId` INT NOT NULL,
    nodeId INT NOT NULL,
    typeId INT NOT NULL,
    PRIMARY KEY (`groupId` , nodeId),
    FOREIGN KEY (`groupId`)
        REFERENCES `Group` (id)
        ON DELETE CASCADE,
    FOREIGN KEY (nodeId)
        REFERENCES Node (id)
        ON DELETE CASCADE,
    FOREIGN KEY (typeId)
        REFERENCES PermissionType (id)
        ON DELETE CASCADE
);

CREATE TABLE `Difficulty` (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    `rank` INT NOT NULL,
    PRIMARY KEY (ID)
);

INSERT INTO `Difficulty` VALUES (1, "Hard", 30);
INSERT INTO `Difficulty` VALUES (2, "Medium", 20);
INSERT INTO `Difficulty` VALUES (3, "Easy", 10);


CREATE TABLE `Exercise` (
    id INT NOT NULL AUTO_INCREMENT,
    creatorId int NOT NULL,
    name VARCHAR(255) NOT NULL DEFAULT '',
    question VARCHAR(512) NOT NULL,
    questionImage VARCHAR(255),
    answer VARCHAR(512) NOT NULL,
    answerImage VARCHAR(255),
    difficultyId INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (difficultyId)
        REFERENCES Difficulty (id),
    FOREIGN KEY (creatorId)
        REFERENCES User (id)
);

CREATE TABLE `NodeExercise` (
    nodeId INT NOT NULL,
    exerciseId INT NOT NULL,
    PRIMARY KEY (nodeId , exerciseId),
    FOREIGN KEY (nodeId)
        REFERENCES Node (id)
        ON DELETE CASCADE,
    FOREIGN KEY (exerciseId)
        REFERENCES Exercise (id)
        ON DELETE CASCADE
);

CREATE TABLE `ExerciseSolved` (
    id INT NOT NULL AUTO_INCREMENT,
    userId INT NOT NULL,
    exerciseId INT NOT NULL,
    `datetime` DATETIME NOT NULL,
    correct BOOL NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (userId)
        REFERENCES User (id)
        ON DELETE CASCADE,
    FOREIGN KEY (exerciseId)
        REFERENCES Exercise (id)
        ON DELETE CASCADE
);
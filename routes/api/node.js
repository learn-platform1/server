const router = require('express').Router();
const models = require("../../models/index").models;
const sequelize = require("../../models/index").sequelize;
const isAuth = require("../../auth-middleware/index").isAuth;
const imageDirectory = "./images/";
const { v4: uuidv4 } = require('uuid');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const nodeFunction = require('../../functions/node');

router.get('/',
    isAuth,
    (req, res, next) => {
        nodeFunction.getAllAccessibleNodes(req.user.id)
            .then(allNodes => {
                if (!allNodes || !allNodes.length) {
                    res.status(404).send();
                    next(false);
                    return;
                }

                var onlyNodes = allNodes.map(x => {
                    return {
                        id: x.id,
                        name: x.name,
                        creatorId: x.creatorId,
                        parentNodeId: x.parentNodeId
                    }
                });

                res.json(onlyNodes).status(200);
            })
    }
);

router.get('/:nodeId',
    isAuth,
    canView,
    (req, res, next) => {
        nodeFunction.getAllAccessibleNodes(req.user.id)
            .then(allNodes => {
                var foundNode = allNodes.map(x => {
                    return {
                        id: x.id,
                        name: x.name,
                        creatorId: x.creatorId,
                        parentNodeId: x.parentNodeId
                    }
                })
                    .find(x => x.id == req.params.nodeId)
                if (foundNode)
                    res.json(foundNode).status(200);
                else
                    res.status(404);
            })
    }
);

router.post('/',
    isAuth,
    async (req, res, next) => {
        try {
            nodeFunction.getAllAccessibleNodes(req.user.id).then(allNodes => {
                if (req.body.parentNodeId) {
                    var parentNode = allNodes.find(x => x.id == req.body.parentNodeId)
                    if (!parentNode) {
                        res.status(404).send();
                        next(false);
                    }

                }

                models.node.create({
                    name: req.body.name,
                    parentNodeId: req.body.parentNodeId,
                    creatorId: req.user.id
                })
                    .then((newNode) => {
                        res.status(201).json(newNode);
                    })
            })
        } catch (err) {
            next(err);
        }

    }
);

router.delete('/:nodeId',
    isAuth,
    isCreator,
    (req, res, next) => {
        nodeFunction.getAllAccessibleNodes(req.user.id)
            .then(allNodes => {
                var nodeToDelete = allNodes.find(x => x.id == req.params.nodeId);
                if (!nodeToDelete) {
                    res.status(404).send();
                    next(false);
                }
                if (allNodes.some(x => x.parentNodeId == req.params.nodeId) &&
                    !req.body.force) {
                    res.status(400).send();
                    next(false);
                    return;
                }

                nodeToDelete.destroy()
                    .then((destroyed) => {
                        res.status(200).send();
                    })
                    .catch(err => {
                        next(err);
                    })
            })
    }
);

router.get('/:nodeId/permission/group',
    isAuth,
    isCreator,
    async (req, res, next) => {
        var result = [];
        const parentNodeIds = await getAllParentNodeIds(req.params.nodeId);
        const basePermissions = await getAllGroupPermissionForNode(req.params.nodeId);
        var parentPermissions = [];
        for (const parentNodeId of parentNodeIds) {
            parentPermissions.push(await getAllGroupPermissionForNode(parentNodeId));
        }
        var allGroups = [];
        //add parent groups to group array
        for (const parentPermission of parentPermissions) {
            for (const groupPermission of parentPermission) {
                if (allGroups.indexOf(groupPermission.group) === -1) {
                    allGroups.push(groupPermission.group);
                }
            }
        }
        //add base groups to group array
        for (const basePermission of basePermissions) {
            if (allGroups.indexOf(basePermission.group) === -1) {
                allGroups.push(basePermission.group);
            }
        }

        for (const group of allGroups) {
            var baseForThisGroup = basePermissions.find(x => x.group.id == group.id)
            var base = baseForThisGroup ? baseForThisGroup.permission : {};
            var inherited = await getLowestPermissionForGroup(parentPermissions, group);
            result.push({
                group: group,
                node: base ? base : {},
                inherited: inherited ? inherited : {}
            });
        }
        res.json(result).status(200).send();
    }
);

router.post('/:nodeId/permission/group/:groupId',
    isAuth,
    isCreator,
    (req, res, next) => {
        models.grouppermission.findAll({
            where: {
                nodeId: req.params.nodeId,
                groupId: req.params.groupId
            }
        })
            .then((groupPermissions) => {
                if (groupPermissions && groupPermissions.length > 0) {
                    //update
                    groupPermissions[0].update({ typeId: req.body.permissionTypeId })
                        .then((update) => {
                            res.status(200).send();
                        })
                        .catch((err) => {
                            res.status(404).send();
                        })
                }
                else {
                    models.grouppermission.create({
                        nodeId: req.params.nodeId,
                        groupId: req.params.groupId,
                        typeId: req.body.permissionTypeId
                    })
                        .then((insert) => {
                            res.status(200).send();
                        })
                        .catch((err) => {
                            res.status(404).send();
                        })
                }
            })
            .catch((err) => {
                res.status(404).send();
            })
    }
);

router.delete('/:nodeId/permission/group/:groupId',
    isAuth,
    isCreator,
    (req, res, next) => {
        models.grouppermission.findAll({
            where: {
                nodeId: req.params.nodeId,
                groupId: req.params.groupId
            }
        })
            .then((foundPermission) => {
                if (!foundPermission || foundPermission.length == 0)
                    res.status(404).send();

                foundPermission[0].destroy()
                    .then((result) => {
                        res.status(200).send()
                    })
                    .catch((err) => {
                        res.status(404).send();
                    })
            })
    }
);

router.get('/:nodeId/exam',
    isAuth,
    canView,
    async (req, res, next) => {
        var childs = await getChildIdsRecursively(req.params.nodeId);
        childs.push(req.params.nodeId);
        var exercises = await getExercisesForNodeIds(childs);
        if (req.query.difficulty)    {
            var exercisesWithCorrectDifficulty = exercises.filter(x =>
                x.difficultyId == req.query.difficulty);
            res.json(exercisesWithCorrectDifficulty).status(200);
        }
        else {
            res.json(exercises).status(200);
        }

    }
);

async function getExercisesForNodeIds(nodeIds) {
    var allExercises = await models.exercise.findAll({
        include: [{
            model: models.nodeexercise,
            where: {
                nodeId: {
                    [Op.in]: nodeIds
                }
            }
        }]
    })
    var onlyExercises = allExercises.map(x => {
        return {
            id: x.id,
            creatorId: x.creatorId,
            name: x.name,
            question: x.question,
            questionImage: x.questionImage,
            answer: x.answer,
            answerImage: x.answerImage,
            difficultyId: x.difficultyId,
        }
    })
    return onlyExercises;
}

async function getChildIdsRecursively(nodeId) {
    var newChilds = await getChildIds(nodeId);
    if (newChilds && newChilds.length > 0) {
        newChilds.forEach(child => {
            var veryNewChildren = getChildIdsRecursively(child)
            newChilds.concat(veryNewChildren);
        });
    }
    return newChilds;
}

async function getChildIds(nodeId) {
    var childNodes = await models.node.findAll({
        where: {
            parentNodeId: nodeId
        }
    })
    return childNodes.map(x => x.id);
}

router.post('/:nodeId/exercise',
    isAuth,
    canView,
    async (req, res, next) => {
        const t = await sequelize.transaction();
        try {
            var questionImageName = null;
            var answerImageName = null;
            if (req.files) {
                questionImageName = req.files.questionImage
                    ? uuidv4() + "." + req.files.questionImage.mimetype.split("/")[1]
                    : null;
                answerImageName = req.files.answerImage
                    ? uuidv4() + "." + req.files.answerImage.mimetype.split("/")[1]
                    : null;
            }
            var newExercise = await models.exercise.create({
                name: req.body.name,
                creatorId: req.user.id,
                question: req.body.question,
                questionImage: questionImageName,
                answer: req.body.answer,
                answerImage: answerImageName,
                difficultyId: req.body.difficultyId
            }, { transaction: t });

            await models.nodeexercise.create({
                nodeId: req.params.nodeId,
                exerciseId: newExercise.id
            }, { transaction: t });

            await MoveUploadedFilesifExistent(req, res, answerImageName, questionImageName);

            await t.commit();
            res.status(201).json(newExercise).send();
        } catch (error) {
            await t.rollback();
            throw error;
        }
    }
);

router.get('/:nodeId/exercise',
    isAuth,
    canView,
    async (req, res, next) => {
        models.exercise.findAll({
            include: {
                model: models.nodeexercise,
                where: {
                    nodeId: req.params.nodeId
                }
            }
        })
            .then(allExercises => {
                var onlyExercises = allExercises.map(x => {
                    return {
                        id: x.id,
                        creatorId: x.creatorId,
                        name: x.name,
                        question: x.question,
                        questionImage: x.questionImage,
                        answer: x.answer,
                        answerImage: x.answerImage,
                        difficultyId: x.difficultyId
                    }
                })
                res.json(onlyExercises).status(200);
            })
            .catch((err) => {
                res.status(404).send();
            })
    }
)

async function MoveUploadedFilesifExistent(req, res, answerImageName, questionImageName) {
    if (!req.files) {
        return;
    }

    if (req.files.questionImage) {
        var err = await req.files.questionImage.mv(imageDirectory + questionImageName);
        if (err) {
            return res.status(500).send(err);
        }

    }
    if (req.files.questionImage) {
        var err = await req.files.answerImage.mv(imageDirectory + answerImageName);
        if (err) {
            return res.status(500).send(err);
        }
    }

}

async function getAllParentNodeIds(nodeId) {
    return new Promise(resolve => {
        var parentNodes = [];
        models.node.findAll()
            .then((allNodes) => {
                currentNode = allNodes.find(x => x.id == nodeId);
                while (currentNode.parentNodeId) {
                    parentNodes.push(currentNode.parentNodeId);
                    currentNode = allNodes.find(x => x.id == currentNode.parentNodeId)
                }
                resolve(parentNodes);
            })
    });
}

async function getAllGroupPermissionForNode(nodeId) {
    return new Promise(resolve => {
        models.grouppermission.findAll({
            where: {
                nodeId: nodeId
            },
            include: [models.permissiontype, models.group]
        })
            .then((results) => {
                var returnValue = results.map(x => {
                    return {
                        group: x.group,
                        permission: x.permissiontype
                    }
                })
                resolve(returnValue);
            })
    });
}

async function getLowestPermissionForGroup(parentPermissions, group) {
    return new Promise(resolve => {
        var lowestPermission = undefined;
        for (const parentPermission of parentPermissions) {
            for (const groupPermission of parentPermission) {
                if (groupPermission.group.id != group.id)
                    continue;
                if (groupPermission.permission &&
                    (lowestPermission == undefined ||
                        groupPermission.permission.rank < lowestPermission.rank)) {
                    lowestPermission = groupPermission.permission;
                }
            }
        }
        resolve(lowestPermission);
    });
}

function isCreator(req, res, next) {
    models.node.findAll({
        where: {
            id: req.params.nodeId,
            creatorId: req.user.id
        }
    })
        .then((node) => {
            if (node && node.length == 1) {
                next();
            } else {
                res.status(404);
            }

        })
}
async function canView(req, res, next) {
    try {

        var allNodes = await nodeFunction.getAllAccessibleNodes(req.user.id);

        var canView = allNodes.some(x => x.id == req.params.nodeId);
        if (canView)
            next();
        else
            res.status(404).send();
    } catch (error) {
        res.status(404).send();
        throw error;
    }
}

module.exports = router;
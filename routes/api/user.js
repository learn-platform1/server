const router = require('express').Router();
const models = require("../../models/index").models;
const passport = require("passport");
const isAuth = require("../../auth-middleware/index").isAuth;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


router.get('/',
    isAuth,
    (req, res, next) => {
        var username = req.query.username;
        var usingQuery = username != undefined;
        if (usingQuery) {
            username = username.replace('*', '%');
        } else {
            username = req.user.username;
        }

        models.user.findAll({
            where: {
                username: {
                    [Op.like]: username
                }
            }
        })
            .then((users) => {
                console.log(usingQuery)
                if (users == undefined || !users.length)
                    res.json([]).status(404);
                else {
                    res.json(
                        users.map(x => {
                            return {
                                id: x.id,
                                username: x.username,
                                email: usingQuery && x.username != req.user.username
                                    ? null : x.email
                            }
                        })
                    ).status(200);
                }
            })
            .catch((err) => {
                next(err)
            })
    }
);

router.post('/login',
    passport.authenticate('local-login', { failureRedirect: '/login' }),
    (req, res, next) => {
        res.status(200).send();
    }
);

router.post('/register',
    passport.authenticate('local-register', { failureRedirect: '/register' }),
    (req, res, next) => {
        res.status(201).send();
    }
);

router.post('/logout',
    isAuth,
    (req, res, next) => {
        req.logout();
        res.status(200).send();
    }
);

router.get('/group',
    isAuth,
    (req, res, next) => {
        models.usergroup.findAll({
            where: {
                userId: req.user.id
            },
            include: models.group
        })
            .then((groups) => {
                res.json(groups.map(x => x.group)).status(200);
            })
            .catch((err) => {
                next(err);
            })
    }
);

module.exports = router;
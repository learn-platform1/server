const router = require('express').Router();
const models = require("../../models/index").models;
const isAuth = require("../../auth-middleware/index").isAuth;

router.post('/',
    isAuth,
    (req, res, next) => {
        models.group.create({
            name: req.body.name,
            creatorId: req.user.id
        })
            .then((newGroup) => {

                models.usergroup.create({
                    userId: req.user.id,
                    groupId: newGroup.id
                })
                    .then((userGroupEntry) => {
                        res.json(newGroup).status(201);
                    })
                    .catch((err) => {
                        next(err);
                    })
            })
            .catch((err) => {
                res.status(409).send();
                next(err);
            })
    }
);

router.put('/:groupId',
    isAuth,
    (req, res, next) => {
        models.group.findAll({
            where: {
                id: req.params.groupId
            }
        })
            .then((groups) => {
                if (!groups || !groups.length)
                    next(err);

                groups[0].name = req.body.name;
                groups[0].save();
                res.json(groups[0]).status(200);
            })
            .catch((err) => {
                next(err);
            })
    }
);

router.delete('/:groupId',
    isAuth,
    (req, res, next) => {
        models.group.findAll({
            where: {
                id: req.params.groupId
            }
        })
            .then((groups) => {
                if (!groups || !groups.length)
                    res.status(404).send();
                if (groups[0].creatorId != req.user.id)
                    res.status(404).send();

                groups[0].destroy()
                    .then((destroyed) => {
                        res.status(200).send();
                    });
            })
            .catch((err) => {
                next(err);
            })
    }
);

router.get('/:groupId/user',
    isAuth,
    (req, res, next) => {
        models.group.findAll({
            where: {
                id: req.params.groupId
            },
            include: models.usergroup
        })
            .then((groups) => {
                if (!groups || !groups.length)
                    res.status(404).send();
                if (groups[0].creatorId != req.user.id)
                    res.status(404).send();
                var group = groups[0];
                var userIdsInGroup = group.usergroups.map(x => x.userId);
                models.user.findAll()
                    .then((users) => {
                        var usersInGroup = users.filter(x => userIdsInGroup.includes(x.id));
                        var reducedInfoUsers = usersInGroup.map(x => {
                            return { id: x.id, username: x.username }
                        });
                        res.status(200).json(reducedInfoUsers);
                    })
            });
    }
);

router.post('/:groupId/invite/:userId',
    isAuth,
    (req, res, next) => {
        models.group.findAll({
            where: {
                id: req.params.groupId
            }
        })
            .then((groups) => {
                if (!groups || !groups.length)
                    res.status(404).send();
                if (groups[0].creatorId != req.user.id)
                    res.status(404).send();

                models.usergroup.create({
                    userId: req.params.userId,
                    groupId: req.params.groupId
                })
                    .then((newUserGroupEntry) => {
                        res.status(200).send();
                    })
                    .catch((err) => {
                        next(err);
                    })

            })
            .catch((err) => {
                next(err);
            })
    }
);

router.post('/:groupId/kick/:userId',
    isAuth,
    (req, res, next) => {
        models.group.findAll({
            where: {
                id: req.params.groupId
            }
        })
            .then((groups) => {
                if (!groups || !groups.length)
                    res.status(404).send();
                if (groups[0].creatorId != req.user.id)
                    res.status(404).send();
                if (req.user.id == req.params.userId)
                    res.status(400).send();

                models.usergroup.findAll({
                    where: {
                        groupId: req.params.groupId,
                        userId: req.params.userId
                    }
                })
                    .then((userGroupEntry) => {
                        if (!userGroupEntry || !userGroupEntry.length)
                            res.status(404).send();

                        userGroupEntry[0].destroy()
                            .then((destroyed) => {
                                res.status(200).send();
                            })
                            .catch((err) => {
                                next(err);
                            })
                    })
                    .catch((err) => {
                        next(err);
                    })

            })
            .catch((err) => {
                next(err);
            })
    }
);

module.exports = router;
const router = require('express').Router();
const models = require("../../models/index").models;
const sequelize = require("../../models/index").sequelize;
const isAuth = require("../../auth-middleware/index").isAuth;

router.get('/',
    isAuth,
    (req, res, next) => {
        models.difficulty.findAll()
        .then(allDificulties => {
            res.json(allDificulties).status(200);
        })
    }
);

module.exports = router;
var router = require('express').Router();

router.use('/user', require('./user'));
router.use('/group', require('./group'));
router.use('/node', require('./node'));
router.use('/permission', require('./permission'));
router.use('/exercise', require('./exercise'));
router.use('/difficulty', require('./difficulty'));
router.use('/image', require('./image'));

router.use((err, req, res, next) => {
  if (err.name === 'ValidationError') {
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function (errors, key) {
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});
module.exports = router;
const router = require('express').Router();
const models = require("../../models/index").models;
const sequelize = require("../../models/index").sequelize;
const isAuth = require("../../auth-middleware/index").isAuth;
const imageDirectory = "./images/";
const { v4: uuidv4 } = require('uuid');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const nodeFunction = require('../../functions/node');

router.get('/',
    isAuth,
    (req, res, next) => {
        models.permissiontype.findAll()
        .then(types => {
            res.json(types).status(200);
        })
    })

module.exports = router; module.exports = router;
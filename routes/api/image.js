const router = require('express').Router();
const isAuth = require("../../auth-middleware/index").isAuth;
const imageDirectory = "./images/";
const resolve = require('path').resolve


router.get('/:imageId',
    isAuth,
    (req, res, next) => {
        res.sendFile(resolve(imageDirectory + req.params.imageId));
    }
);

module.exports = router;
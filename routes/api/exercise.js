const router = require('express').Router();
const models = require("../../models/index").models;
const isAuth = require("../../auth-middleware/index").isAuth;
const imageDirectory = "./images/";
const { v4: uuidv4 } = require('uuid');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const nodeFunction = require('../../functions/node')

router.get('/',
    isAuth,
    async (req, res, next) => {
        var nodes = await nodeFunction.getAllAccessibleNodes(req.user.id);
        var nodeIds = nodes.map(x => x.id);
        var allExercises = await models.exercise.findAll({
            include: [{
                model: models.nodeexercise,
                where: {
                    nodeId: {
                        [Op.in]: nodeIds
                    }
                }
            }]
        })
        var onlyExercises = allExercises.map(x => {
            return {
                id: x.id,
                creatorId: x.creatorId,
                name: x.name,
                question: x.question,
                questionImage: x.questionImage,
                answer: x.answer,
                answerImage: x.answerImage,
                difficultyId: x.difficultyId
            }
        })

        res.json(onlyExercises).status(200);
    }
);

router.get('/:exerciseId',
    isAuth,
    canView,
    (req, res, next) => {
        models.exercise.findAll({
            where: {
                id: req.params.exerciseId
            }
        })
            .then((exercises) => {
                var foundExercise = exercises[0];
                res.json(foundExercise).status(200).send();
            });
    }
);

router.delete('/:exerciseId',
    isAuth,
    canView,
    (req, res, next) => {
        models.exercise.findAll({
            where: {
                id: req.params.exerciseId
            }
        })
            .then((exercises) => {
                exercises[0].destroy()
                    .then((destroyed) => {
                        res.status(200).send();
                    })
                    .catch((err) => {
                        next(err);
                    })
            })
    }
);

router.put('/:exerciseId',
    isAuth,
    canView,
    (req, res, next) => {
        models.exercise.findAll({
            where: {
                id: req.params.exerciseId
            }
        })
            .then(async (exercises) => {
                var questionImageName = null;
                var answerImageName = null;
                if (req.files) {
                    questionImageName = req.files.questionImage
                        ? uuidv4() + "." + req.files.questionImage.mimetype.split("/")[1]
                        : null;
                    answerImageName = req.files.answerImage
                        ? uuidv4() + "." + req.files.answerImage.mimetype.split("/")[1]
                        : null;
                }
                var exercise = exercises[0];
                exercise.name = req.body.name ? req.body.name : exercise.name;
                exercise.question = req.body.question ? req.body.question : exercise.question;
                exercise.answer = req.body.answer ? req.body.answer : exercise.answer;
                exercise.difficultyId = req.body.difficultyId ? req.body.difficultyId : exercise.difficultyId;
                exercise.questionImage = questionImageName ? questionImageName : exercise.questionImage;
                exercise.answerImage = answerImageName ? answerImageName : exercise.answerImage;

                exercise.save();

                await MoveUploadedFilesifExistent(req, res, questionImageName, answerImageName);
            })
    }
);

router.post('/:exerciseId',
    isAuth,
    canView,
    (req, res, next) => {
        models.exercisesolved.create({
            userId: req.user.id,
            exerciseId: req.params.exerciseId,
            datetime: Date.now(),
            correct: req.body.correct == "true"
        })
            .then((exerciseSolved) => {
                res.status(200).send();
            })
            .catch((err) => {
                next(err);
            })
    }
);

async function canView(req, res, next) {
    try {

        var allExercises = await nodeFunction.getAllAccessibleExercises(req.user.id);
        var canView = allExercises.some(x => x.id == req.params.exerciseId);
        if (canView)
            next();
        else
            res.status(404).send();
    } catch (error) {
        res.status(404).send();
        throw error;
    }
}

async function MoveUploadedFilesifExistent(req, res, answerImageName, questionImageName) {
    if (!req.files) {
        return;
    }

    if (req.files.questionImage) {
        var err = await req.files.questionImage.mv(imageDirectory + questionImageName);
        if (err) {
            return res.status(500).send(err);
        }

    }
    if (req.files.questionImage) {
        var err = await req.files.answerImage.mv(imageDirectory + answerImageName);
        if (err) {
            return res.status(500).send(err);
        }
    }
}

module.exports = router;
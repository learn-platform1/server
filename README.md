# Learn Platform

## How to run

1. `npm i`
2. create .env File with needed variables

| Name | Description |
|--|--|
| NODE_ENV | What Environment to run in. (eg. development) |
| PORT | What port to run on. |
| DB_HOST | Where the Database is. |
| DB_USER | Database user |
| DB_PASSWORD | Database password |
| DB_NAME | Database name |  

3. `node index.js` or `nodemon index.js`  

## To regenerate the models

`node_modules/.bin/sequelize-auto -h localhost -d LearnPlatform -u user -x password -p 3306  --dialect mysql -o ./models`

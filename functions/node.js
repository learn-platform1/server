const models = require("../models/index").models;

async function getAllAccessibleNodes(userId) {
    return new Promise((resolve) => {
        getAllGroupsFromUser(userId)
            .then(groups => {
                models.node.findAll({
                    include: [models.grouppermission]
                })
                    .then(async (result) => {
                        if (!result || !result.length)
                            resolve([]);

                        var permissionTypes = await getAllPermissionTypes();
                        var filteredResult = result.filter(x => getUserPermission(
                            groups, permissionTypes, result, x
                        ));

                        resolve(filteredResult);
                    })
            })
    })
}

async function getAllAccessibleExercises(userId) {
    return new Promise(async (resolve) => {
        var allNodes = await getAllAccessibleNodes(userId);
        var ids = allNodes.map(x => x.id);


        models.nodeexercise.findAll({
            where: {
                nodeId: ids
            },
            include: [models.exercise]
        })
            .then(allAccessibleNodeExercises => {
                var onlyExercises = allAccessibleNodeExercises.map(x => {
                    return {
                        id: x.exercise.id,
                        creatorId: x.exercise.creatorId,
                        name: x.exercise.name,
                        question: x.exercise.question,
                        questionImage: x.exercise.questionImage,
                        answer: x.exercise.answer,
                        answerImage: x.exercise.answerImage,
                        difficultyId: x.exercise.difficultyId
                    }
                })
                resolve(onlyExercises);
            })
    })
}

async function getAllGroupsFromUser(userId) {
    return new Promise(resolve => {
        models.group.findAll({
            include: {
                model: models.usergroup,
                where: {
                    userId: userId
                }
            }
        })
            .then((groups) => {
                resolve(groups);
            })
            .catch((err) => {
                resole([]);
            })
    })
}

/**
 * Returns the permissionType object with the lowest rank
 */
function getUserPermission(userGroups, permissionTypes, allNodes, nodeToTest) {
    var currentNode = nodeToTest;
    var lowestGroupPermission = undefined;
    do {
        if (currentNode.grouppermssions) {
            var groupPermissions = currentNode.grouppermssions.filter(x => userGroups.includes(x));
            if (groupPermissions.length > 0) {
                groupPermissions.forEach(groupPermission => {
                    var groupPermission = permissionTypes.find(x => x.id == groupPermission.typeId);
                    if (groupPermission.rank < lowestGroupPermission ? lowestGroupPermission.rank : 9999) {
                        lowestGroupPermission = groupPermission;
                    }
                });
            }
        }

        currentNode = allNodes.find(x => x.id == currentNode.parentNodeId);
    } while (currentNode)

    return lowestGroupPermission ? lowestGroupPermission.rank : 9999;
}

async function getAllPermissionTypes() {
    return new Promise(resolve => {
        models.permissiontype.findAll()
            .then(types => {
                resolve(types);
            })
            .catch(err => {
                resolve([]);
            })
    });
}

module.exports = { getAllAccessibleNodes, getAllAccessibleExercises, getAllGroupsFromUser, getUserPermission, getAllPermissionTypes };
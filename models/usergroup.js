const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('usergroup', {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    groupId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'group',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'usergroup',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userId" },
          { name: "groupId" },
        ]
      },
      {
        name: "groupId",
        using: "BTREE",
        fields: [
          { name: "groupId" },
        ]
      },
    ]
  });
};

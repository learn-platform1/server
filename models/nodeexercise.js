const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nodeexercise', {
    nodeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'node',
        key: 'id'
      }
    },
    exerciseId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'exercise',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'nodeexercise',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nodeId" },
          { name: "exerciseId" },
        ]
      },
      {
        name: "exerciseId",
        using: "BTREE",
        fields: [
          { name: "exerciseId" },
        ]
      },
    ]
  });
};

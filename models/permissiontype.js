const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('permissiontype', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "name"
    },
    rank: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: "rank"
    }
  }, {
    sequelize,
    tableName: 'permissiontype',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "rank",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "rank" },
        ]
      },
    ]
  });
};

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exercise', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    creatorId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    question: {
      type: DataTypes.STRING(512),
      allowNull: false
    },
    questionImage: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    answer: {
      type: DataTypes.STRING(512),
      allowNull: false
    },
    answerImage: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    difficultyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'difficulty',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'exercise',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "difficultyId",
        using: "BTREE",
        fields: [
          { name: "difficultyId" },
        ]
      },
      {
        name: "creatorId",
        using: "BTREE",
        fields: [
          { name: "creatorId" },
        ]
      },
    ]
  });
};

var DataTypes = require("sequelize").DataTypes;
var _difficulty = require("./difficulty");
var _exercise = require("./exercise");
var _exercisesolved = require("./exercisesolved");
var _group = require("./group");
var _grouppermission = require("./grouppermission");
var _node = require("./node");
var _nodeexercise = require("./nodeexercise");
var _permissiontype = require("./permissiontype");
var _user = require("./user");
var _usergroup = require("./usergroup");

function initModels(sequelize) {
  var difficulty = _difficulty(sequelize, DataTypes);
  var exercise = _exercise(sequelize, DataTypes);
  var exercisesolved = _exercisesolved(sequelize, DataTypes);
  var group = _group(sequelize, DataTypes);
  var grouppermission = _grouppermission(sequelize, DataTypes);
  var node = _node(sequelize, DataTypes);
  var nodeexercise = _nodeexercise(sequelize, DataTypes);
  var permissiontype = _permissiontype(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);
  var usergroup = _usergroup(sequelize, DataTypes);

  node.belongsToMany(group, { through: grouppermission, foreignKey: "nodeId", otherKey: "groupId" });
  group.belongsToMany(node, { through: grouppermission, foreignKey: "groupId", otherKey: "nodeId" });
  exercise.belongsToMany(node, { through: nodeexercise, foreignKey: "exerciseId", otherKey: "nodeId" });
  node.belongsToMany(exercise, { through: nodeexercise, foreignKey: "nodeId", otherKey: "exerciseId" });
  group.belongsToMany(user, { through: usergroup, foreignKey: "groupId", otherKey: "userId" });
  user.belongsToMany(group, { through: usergroup, foreignKey: "userId", otherKey: "groupId" });
  exercise.belongsTo(difficulty, { foreignKey: "difficultyId"});
  difficulty.hasMany(exercise, { foreignKey: "difficultyId"});
  exercise.belongsTo(user, { foreignKey: "creatorId"});
  user.hasMany(exercise, { foreignKey: "creatorId"});
  exercisesolved.belongsTo(user, { foreignKey: "userId"});
  user.hasMany(exercisesolved, { foreignKey: "userId"});
  exercisesolved.belongsTo(exercise, { foreignKey: "exerciseId"});
  exercise.hasMany(exercisesolved, { foreignKey: "exerciseId"});
  group.belongsTo(user, { foreignKey: "creatorId"});
  user.hasMany(group, { foreignKey: "creatorId"});
  grouppermission.belongsTo(group, { foreignKey: "groupId"});
  group.hasMany(grouppermission, { foreignKey: "groupId"});
  grouppermission.belongsTo(node, { foreignKey: "nodeId"});
  node.hasMany(grouppermission, { foreignKey: "nodeId"});
  grouppermission.belongsTo(permissiontype, { foreignKey: "typeId"});
  permissiontype.hasMany(grouppermission, { foreignKey: "typeId"});
  node.belongsTo(node, { foreignKey: "parentNodeId"});
  node.hasMany(node, { foreignKey: "parentNodeId"});
  node.belongsTo(user, { foreignKey: "creatorId"});
  user.hasMany(node, { foreignKey: "creatorId"});
  nodeexercise.belongsTo(node, { foreignKey: "nodeId"});
  node.hasMany(nodeexercise, { foreignKey: "nodeId"});
  nodeexercise.belongsTo(exercise, { foreignKey: "exerciseId"});
  exercise.hasMany(nodeexercise, { foreignKey: "exerciseId"});
  usergroup.belongsTo(user, { foreignKey: "userId"});
  user.hasMany(usergroup, { foreignKey: "userId"});
  usergroup.belongsTo(group, { foreignKey: "groupId"});
  group.hasMany(usergroup, { foreignKey: "groupId"});

  return {
    difficulty,
    exercise,
    exercisesolved,
    group,
    grouppermission,
    node,
    nodeexercise,
    permissiontype,
    user,
    usergroup,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;

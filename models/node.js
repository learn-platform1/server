const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('node', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    parentNodeId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'node',
        key: 'id'
      }
    },
    creatorId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'node',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "parentNodeId",
        using: "BTREE",
        fields: [
          { name: "parentNodeId" },
        ]
      },
      {
        name: "creatorId",
        using: "BTREE",
        fields: [
          { name: "creatorId" },
        ]
      },
    ]
  });
};

var LocalStrategy = require('passport-local').Strategy;
const models = require("../models/index").models;
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = (passport) => {
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        models.user.findAll({
            where: {
                id: id
            }
        }).then((users) => {
            if (!users.length || users == undefined)
                done(null, false);
            done(null, users[0])
        }).catch((err) => {
            done(err);
        })
    });

    passport.use('local-register', new LocalStrategy({
        passReqToCallback: true
    },
        (req, username, password, done) => {
            models.user.findAll({
                where: {
                    username: username
                }
            }).then((users) => {
                if (users.length)
                    return done(null, false);

                bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
                    if (err)
                        return done(err);
                    models.user.create({
                        username: username,
                        password: hashedPassword,
                        email: req.body.email || undefined
                    })
                        .then((newUser) => {
                            return done(null, newUser);
                        })
                        .catch((err) => {
                            return done(err);
                        })
                });
            })
                .catch((err) => {
                    return done(err);
                })
        }
    ));

    passport.use('local-login', new LocalStrategy({
        passReqToCallback: true
    },
        (req, username, password, done) => {
            models.user.findAll({
                where: {
                    username: username
                }
            }).then((users) => {
                if (!users.length || users == undefined)
                    return done(null, false);

                bcrypt.compare(password, users[0].password, function (err, result) {
                    if (err)
                        return done(err);

                    if (!result)
                        return done(null, false);

                    return done(null, users[0]);
                });
            })
                .catch((err) => {
                    done(err);
                })
        }));
};
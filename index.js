const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const session = require('express-session')
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const models = require("./models/index").models;
const passport = require('passport');
require("./config/passport")(passport)
const fileUpload = require('express-fileupload')

const app = express();

app.use(morgan('dev'));
app.use(cors({
  credentials: true,
  origin: "http://localhost:8080"
}));
app.use(bodyParser.json());
app.use(session({
  secret: 'some secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24
  }
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(fileUpload());

app.use(require('./routes'));

require("./error-handlers")(app);

const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});